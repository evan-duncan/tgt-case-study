require('dotenv').config();
const data = require('./item-data.json');
const express = require('express');

const app = express();

function getLatency() {
  return Math.floor(Math.random() * 750) + 250;
}

app.set('API_PORT', process.env.API_PORT);

app.get('/api/healthz', (req, res) => res.send('OK'));
app.get('/api/items', (req, res) =>
  setTimeout(() => res.json([data]), getLatency()));
app.get('/api/items/:id', (req, res) =>
  setTimeout(() => res.json(data), getLatency()));

app.listen(app.get('API_PORT'), () => console.log(`API running on port ${app.get('API_PORT')}`));
 