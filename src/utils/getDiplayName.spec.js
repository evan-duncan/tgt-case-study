import React from 'react';
import getDisplayName from './getDisplayName';

describe('getDisplayName', () => {
  let component;

  beforeEach(() => {
    component = new React.Component();
  });

  it('should use "diplayName" if it is defined', () => {
    component.displayName = 'foo';
    expect(getDisplayName(component)).toEqual('foo');
  });

  it('should use "name" if defined and "displayName" is undefined', () => {
    component.displayName = undefined;
    component.name = 'bar';
    expect(getDisplayName(component)).toEqual('bar');
  });

  it('should return "Component" if "displayName" and "name" are undefined', () => {
    component.displayName = undefined;
    component.name = undefined;
    expect(getDisplayName(component)).toEqual('Component');
  });
});
