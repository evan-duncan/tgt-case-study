
export default function getDimensions(element) {
  if (!element) {
    return { x: 0, y: 0 };
  }
  const { height: y, width: x } = element.getBoundingClientRect();
  return { x, y };
}
