import React, { Fragment } from 'react';
import styles from './App.module.css';
import asProductSection from './hocs/asProductSection';
import inContainer from './hocs/inContainer';
import Title from './components/Title';
import Image from './components/Image';
import Loading from './components/Loading';
import Carousel from './components/Carousel';
import Promotions from './components/Promotions';
import Quantity from './components/Quantity';
import Highlights from './components/Highlights';
import CheckOutButtons from './features/CheckOutButtons';
import Reviews from './components/Reviews';
import ReturnPolicy from './components/ReturnPolicy/ReturnPolicy';
import Price from './components/Price';
import Actions from './components/Actions/Actions';

const PromotionsContainer = asProductSection(Promotions);
const HeroContainer = asProductSection(Fragment);
const DetailsContainer = inContainer(Fragment);

export default function MobileApp({
  isLoading,
  title,
  activeImage,
  images,
  updateImage,
  offer,
  promotions,
  quantity,
  updateQuantity,
  purchasingChannelCode,
  returnPolicy,
  highlights,
  pro,
  con,
  reviews,
  consolidatedOverallRating,
  totalReviews,
}) {
  return (
    <div className={styles.App}>
      {isLoading
        ? <Loading position="absolute">Loading...</Loading>
        : <Fragment>
          <HeroContainer>
            <div className={styles.centered}>
              <Title>{title}</Title>
              <div className={styles.activeImage}>
                <Image src={activeImage} alt="" />
              </div>
              <Carousel
                images={images}
                onChange={updateImage}
                onItemClick={updateImage}
              />
            </div>
            <Price amount={offer.formattedPriceValue} channel={offer.priceQualifier} />
          </HeroContainer>
          <PromotionsContainer promotions={promotions} />
          <DetailsContainer>
            <Quantity
              count={quantity}
              onDecrement={() => updateQuantity(-1)}
              onIncrement={() => updateQuantity(1)}
            />
            <CheckOutButtons purchasingChannelCode={purchasingChannelCode} />
            <ReturnPolicy {...returnPolicy} app={'#root'} />
            <Actions actions={[
              { text: 'add to registry', onClick: () => {} },
              { text: 'add to list', onClick: () => {} },
              { text: 'share', onClick: () => {} },
            ]} />
            <Highlights headline="product highlights" items={highlights} />
            <Reviews
              pro={pro}
              con={con}
              reviews={reviews}
              overall={consolidatedOverallRating}
              total={totalReviews}
            />
          </DetailsContainer>
        </Fragment>}
    </div>
  );
}
