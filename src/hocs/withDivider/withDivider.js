import React, { Fragment } from 'react';
import Divider from '../../components/Divider';
import getDisplayName from '../../utils/getDisplayName';

export default function withDivider(WrappedComponent) {
  const func = props =>
    <Fragment>
        <WrappedComponent {...props} />
        <Divider />
    </Fragment>

  func.displayName = `WithDivider(${getDisplayName(WrappedComponent)})`;
  return func;
}
