import React from 'react';
import styles from './inContainer.module.css';
import getDisplayName from '../../utils/getDisplayName';

export default function inContainer(WrappedComponent) {
  const func = props => (
    <div className={styles.container}>
      <WrappedComponent {...props} />
    </div>
  );
  func.displayName = `InContainer(${getDisplayName(WrappedComponent)})`;
  return func;
}