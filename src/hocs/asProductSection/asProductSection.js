import { compose } from 'recompose';
import inContainer from '../inContainer';
import withDivider from '../withDivider';

export default compose(withDivider, inContainer);
