import React from 'react';
import renderer from 'react-test-renderer';
import Actions from './Actions';

describe('Actions', () => {
  it("should render correctly", () => {
    const a = renderer
      .create(
        <Actions
          actions={[
            { text: 'Do a thing', onClick: () => alert('yo')}
          ]}
        />)
      .toJSON();
    expect(a).toMatchSnapshot();
  });
});
