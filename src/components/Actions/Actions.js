import React from 'react'
import PropTypes from 'prop-types';
import styles from './Actions.module.css';
import Button from '../Button';

export default function Actions({ actions }) {
  return (
    <div className={styles.actions}>
      {actions.map(({ text, onClick }, idx) =>
        <Button
          key={idx}
          className={styles.action}
          onClick={onClick}
          size="sm"
          flat>
          {text}
        </Button>
      )}
    </div>
  );
}

Actions.propTypes = {
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
    })),
};
