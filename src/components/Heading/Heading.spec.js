import React from 'react';
import renderer from 'react-test-renderer';
import {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6,
} from './index';

describe('Heading', () => {
  it("should render H1 correctly", () => {
    const h = renderer
      .create(<H1>Yo, dawg</H1>)
      .toJSON();
    expect(h).toMatchSnapshot();
  });

  it("should render H2 correctly", () => {
    const h = renderer
      .create(<H2>Yo, dawg</H2>)
      .toJSON();
    expect(h).toMatchSnapshot();
  });

  it("should render H3 correctly", () => {
    const h = renderer
      .create(<H3>Yo, dawg</H3>)
      .toJSON();
    expect(h).toMatchSnapshot();
  });

  it("should render H4 correctly", () => {
    const h = renderer
      .create(<H4>Yo, dawg</H4>)
      .toJSON();
    expect(h).toMatchSnapshot();
  });

  it("should render H5 correctly", () => {
    const h = renderer
      .create(<H5>Yo, dawg</H5>)
      .toJSON();
    expect(h).toMatchSnapshot();
  });
  
  it("should render H6 correctly", () => {
    const h = renderer
      .create(<H6>Yo, dawg</H6>)
      .toJSON();
    expect(h).toMatchSnapshot();
  });
});
