import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Heading.module.css';

export default function createHeading(level) {
  function Heading({ children, color }) {
    const H = `h${level}`;
    return (
      <H style={{ color }} className={classNames(
        styles.heading,
        styles[H],
      )}>
        {children}
      </H>
    );
  }

  Heading.propTypes = {
    children: PropTypes.node,
    color: PropTypes.string,
  };

  Heading.defaultProps = {
    children: null,
    color: '#000',
  };

  return Heading;
}
