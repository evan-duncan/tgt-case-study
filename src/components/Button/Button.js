import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Button.module.css';

export default function Button({
  children,
  flat,
  onClick,
  size,
  type,
}) {
  return (
    <button
      className={classNames(
        styles.btn,
        styles[type],
        styles[size],
        {
          [styles.flat]: flat,
        }
      )}
      onClick={onClick}>
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node,
  flat: PropTypes.bool,
  onClick: PropTypes.func,
  type: PropTypes.oneOf([
    'default',
    'primary',
    'secondary',
  ]),
  size: PropTypes.oneOf([
    'sm',
    'md',
    'lg',
  ]),
};

Button.defaultProps = {
  children: null,
  flat: false,
  onClick: () => {},
  type: 'default',
  size: 'lg',
};
