import React from 'react';
import Button from './Button';
import renderer from 'react-test-renderer';

describe('Button', () => {
  [
    'default',
    'primary',
    'secondary',
  ].forEach((test) => 
    it(`renders the ${test} button correctly`, () => {
      const btn = renderer
        .create(<Button type={test}>Foo</Button>)
        .toJSON();
      expect(btn).toMatchSnapshot();
    }));
});
