/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { format } from 'date-fns';
import StarRating from '../StarRating';
import { H4 } from '../Heading';


// TODO: Need to update to support FullReviewShape
export default function Review({
  RatableAttributes,
  className,
  datePosted,
  overallRating,
  review,
  reviewKey,
  screenName,
  title,
}) {
  return (
    <div className={className}>
      <StarRating value={Number(overallRating)} />
      <H4>{title}</H4>
      <p>{review}</p>
      <div>
        <a href="#">{screenName}</a>&nbsp;
        <span>{format(new Date(datePosted), 'MMMM D, YYYY')}</span>
      </div>
    </div>
  );
}
