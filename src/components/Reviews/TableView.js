import React, { Fragment } from 'react';
import withDivider from '../../hocs/withDivider';
import styles from './Reviews.module.css';
import { H3 } from '../Heading';
import StarRating from '../StarRating';
import Review from './Review';

const ReviewHeading = withDivider(Fragment);

export default function TableView({
  con,
  overall,
  pro,
  reviews,
  total,
}) {
  return (
    <div>
      <div className={styles.flexWrapper}>
        <div>
          <StarRating size="xl" value={overall} />&nbsp;<span className={styles.overall}>overall</span>
        </div>
        <div>
          <span className={styles.viewAll}>view all {total} reviews</span>
        </div>
      </div>
      <div className={styles.reviewPanel}>
        <ReviewHeading>
          <div className={styles.flexWrapper}>
            <div className={styles.container}>
              <H3>PRO</H3>
              <p>most helpful 4-5 star review</p>
            </div>
            <div className={styles.container}>
              <H3>CON</H3>
              <p>most helpful 1-2 star review</p>
            </div>
          </div>
        </ReviewHeading>
        <div className={styles.flexWrapper}>
          <Review {...pro} className={styles.container} />
          <Review {...con} className={styles.container} />
        </div>
      </div>
    </div>
  );
}
