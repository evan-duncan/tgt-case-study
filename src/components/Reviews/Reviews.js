import React, { Component } from 'react';
import PropTypes from 'prop-types';
import getDimensions from '../../utils/getDimensions';
import TableView from './TableView';
import StackedView from './StackedView';

export default class Reviews extends Component {
  constructor(props) {
    super(props);
    const { x } = getDimensions(document.getElementById('root'));
    this.state = {
      x,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    const { x } = getDimensions(document.getElementById('root'));
    this.setState({ x });
  }
  
  render () {
    const {
      props,
      state: {
        x,
      },
    } = this;

    return x > 650
      ? <TableView {...props} />
      : <StackedView {...props} />
  }
}

const ReviewShape = {
  RatableAttributes: PropTypes.arrayOf(PropTypes.shape({
    description: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  })),
  datePosted: PropTypes.string.isRequired,
  overallRating: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired,
  reviewKey: PropTypes.string.isRequired,
  screenName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

const FullReviewShape = {
  ...ReviewShape,
  city: PropTypes.string.isRequired,
  customerId: PropTypes.string.isRequired,
  helpfulVotes: PropTypes.string.isRequired,
  overallRating: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  totalComments: PropTypes.string.isRequired,
  totalVotes: PropTypes.string.isRequired,
  comments: PropTypes.arrayOf(PropTypes.shape({
    city: PropTypes.string.isRequired,
    commentKey: PropTypes.string.isRequired,
    commentText: PropTypes.string.isRequired,
    postedDate: PropTypes.string.isRequired,
    screenName: PropTypes.string.isRequired,
    userKey: PropTypes.string.isRequired,
    userTier: PropTypes.string.isRequired,
  })),
};

Reviews.propTypes = {
  overall: PropTypes.number,
  pro: PropTypes.shape(ReviewShape).isRequired,
  con: PropTypes.shape(ReviewShape).isRequired,
  reviews: PropTypes.arrayOf(PropTypes.shape(FullReviewShape)),
  total: PropTypes.number,
};

Reviews.defaultProps = {
  overall: null,
  reviews: [],
  total: null,
};
