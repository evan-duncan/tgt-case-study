import React, { Fragment } from 'react';
import withDivider from '../../hocs/withDivider';
import styles from './Reviews.module.css';
import { H3 } from '../Heading';
import StarRating from '../StarRating';
import Review from './Review';

const ReviewHeading = withDivider(Fragment);

export default function TableView({
  con,
  overall,
  pro,
  reviews,
  total,
}) {
  return (
    <div>
      <div className={styles.flexWrapper}>
        <div>
          <div className={styles.stackedOverall}>overall</div>
          <StarRating size="xl" value={overall} />
        </div>
        <div>
          <span className={styles.stackedViewAll}>view all {total} reviews</span>
        </div>
      </div>
      <div className={styles.stackedPanel}>
        <ReviewHeading>
          <H3>PRO</H3>
          <p>most helpful 4-5 star review</p>
        </ReviewHeading>
        <Review {...pro} />
      </div>
      <div className={styles.stackedPanel}>
        <ReviewHeading>
          <H3>CON</H3>
          <p>most helpful 1-2 star review</p>
        </ReviewHeading>
        <Review {...con} />
      </div>
    </div>
  );
}
