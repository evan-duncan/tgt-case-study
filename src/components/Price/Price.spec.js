import React from 'react';
import renderer from 'react-test-renderer';
import Price from './Price';

describe('Price', () => {
  it("should render correctly", () => {
    const p = renderer
      .create(
        <Price amount={100.00} channel={0} />)
      .toJSON();
    expect(p).toMatchSnapshot();
  });
});
