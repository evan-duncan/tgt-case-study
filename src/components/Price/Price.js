import React from 'react'
import styles from './Price.module.css';
import { H2 } from '../Heading';

export default function Price({
  amount,
  channel,
  className,
}) {
  return (
    <div className={className}>
      <H2>{amount}&nbsp;<small className={styles.channel}>{channel}</small></H2>
    </div>
  );
}
