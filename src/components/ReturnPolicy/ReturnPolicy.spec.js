import React from 'react';
import renderer from 'react-test-renderer';
import ReturnPolicy from './ReturnPolicy';

describe('ReturnPolicy', () => {
  it("should render correctly", () => {
    const rp = renderer
      .create(
        <ReturnPolicy
          legalCopy="Yo, dawg"
          ReturnPolicyDetails={[
            {
             "guestMessage": "View our return policy",
             "policyDays": "100",
             "user": "Regular Guest"
            },
            {
             "guestMessage": "View our return policy",
             "policyDays": "120",
             "user": "Best Guest"
            }
           ]}
        />)
      .toJSON();
    expect(rp).toMatchSnapshot();
  });
});
