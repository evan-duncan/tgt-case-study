import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { H3 } from '../Heading';
import Divider from '../Divider';
import styles from './ReturnPolicy.module.css';

// TODO: Get user information from somewhere
function getPolicyFor(user, policies) {
  return policies[0];
}


export default class ReturnPolicy extends Component {

  static propTypes = {
    legalCopy: PropTypes.string.isRequired,
    ReturnPolicyDetails: PropTypes.arrayOf(
      PropTypes.shape({
        guestMessage: PropTypes.string.isRequired,
        policyDays: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired,
      })),
    app: PropTypes.string,
  };

  static defaultProps = {
    ReturnPolicyDetails: [],
    app: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
    };

    if (props.app) {
      Modal.setAppElement(props.app);
    }
  }

  open = () =>
    this.setState({ modalOpen: true });

  close = () =>
    this.setState({ modalOpen: false });

  render() {
    const {
      close,
      open,
      props: {
        legalCopy,
        ReturnPolicyDetails,
      },
      state: {
        modalOpen,
      },
    } = this;
    const { policyDays } = getPolicyFor({}, ReturnPolicyDetails);
    return (
      <div className={styles.wrapper}>
        <H3 color="#666">returns</H3>
        <Divider vertical />
        <span className={styles.disclaimer}>
          This item must be returned within {policyDays} days of the ship date.
          See <button className={styles.viewPolicy} onClick={open}>return policy</button> for details.
          Prices, promotions, styles and availability may vary by store and online.
        </span>
        <Modal
          isOpen={modalOpen}
          onRequestClose={close}
          contentLabel="Return Policy"
          style={{
            content : {
              top: '50%',
              left: '50%',
              right: '10%',
              bottom: '10%',
              marginRight: '-50%',
              transform: 'translate(-50%, -50%)'
            }
          }}>
          <p dangerouslySetInnerHTML={{__html: legalCopy}} />
        </Modal>
      </div>
    )
  }
}
