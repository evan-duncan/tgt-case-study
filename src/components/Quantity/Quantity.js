import React from 'react'
import PropTypes from 'prop-types';
import { FaPlusCircle, FaMinusCircle } from 'react-icons/fa';
import { noop } from 'lodash';
import classNames from 'classnames';
import styles from './Quantity.module.css';

export default function Quantity({ count, onDecrement, onIncrement }) {
  return (
    <div className={styles.quantity}>
      <div>
        <div className={styles.text}>quantity:</div>
      </div>
      <div className={styles.actionWrapper}>
        <div className={classNames(
          styles.button,
          {
            [styles.disabled]: count === 1,
          }
        )}>
          <FaMinusCircle onClick={count === 1 ? noop : onDecrement} />
        </div>
        <div className={styles.count}>{count}</div>
        <div className={styles.button}>
          <FaPlusCircle onClick={onIncrement} />
        </div>
      </div>
    </div>
  );
}

Quantity.propTypes = {
  count: PropTypes.number.isRequired,
  onDecrement: PropTypes.func.isRequired,
  onIncrement: PropTypes.func.isRequired,
};

Quantity.defaultProps = {};
