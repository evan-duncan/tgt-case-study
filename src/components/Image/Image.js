import React from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ImageMagnify from 'react-image-magnify';
import styles from './Image.module.css';

export default function Image({
  alt,
  src,
  thumbnail,
  zoomable,
}) {
  return (zoomable
    ? (<ImageMagnify {...{
      smallImage: {
        alt,
        src,
        width: 400,
        height: 400,
      },
      largeImage: {
        src: `${src}?wid=1600&hei=1600`,
        width: 1600,
        height: 1600,
      },
      style: { display: 'inline-block' },
      imageClassName: styles.image,
      isHintEnabled: true,
      shouldHideHintAfterFirstActivation: false,
    }} />)
    : (<img
      className={classNames(
        styles.image,
        {
          [styles.thumbnail]: thumbnail,
        },
      )}
      src={src}
      alt={alt}
    />)
  );
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  thumbnail: PropTypes.bool,
  zoomable: PropTypes.bool,
};

Image.defaultProps = {
  thumbnail: false,
  zoomable: false,
};
