import React from 'react'
import renderer from 'react-test-renderer';
import Image from './Image';

describe('Image', () => {
  it('should render correctly', () => {
    const i = renderer
      .create(<Image src="https://en.wikipedia.org/wiki/Morris_the_Cat#/media/File:Indian_Cat_pic.jpg" alt="Morris the Cat" />)
      .toJSON();
    expect(i).toMatchSnapshot();
  });
});
