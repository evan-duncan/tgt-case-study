import React from 'react';
import PropTypes from 'prop-types';
import styles from './Title.module.css';

export default function Title({ children, ...rest }) {
  return (
    <div className={styles.title}>
      {children}
    </div>
  );
}

Title.propTypes = {
  children: PropTypes.node,
};

Title.defaultProps = {
  children: null,
};
