import React from 'react'
import renderer from 'react-test-renderer';
import Title from './Title';

describe('Title', () => {
  it('should render correctly', () => {
    const t = renderer
      .create(<Title>Yo, dawg</Title>)
      .toJSON();
    expect(t).toMatchSnapshot();
  });
});
