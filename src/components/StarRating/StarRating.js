import React from 'react';
import PropTypes from 'prop-types';
import ReactStars from 'react-stars';
import styles from './StarRating.module.css';

const sizes = {
  sm: 12,
  md: 16,
  lg: 24,
  xl: 32,
};

export default function StarRating({
  edit,
  size,
  value,
}) {
  return <ReactStars
    className={styles.ratings}
    color1="#989898"
    color2="#CC0000"
    count={5}
    edit={edit}
    size={sizes[size]}
    value={value}
  />;
}

StarRating.propTypes = {
  edit: PropTypes.bool,
  value: PropTypes.number.isRequired,
  size: PropTypes.oneOf([
    'sm',
    'md',
    'lg',
    'xl',
  ]),
};

StarRating.defaultProps = {
  edit: false,
  size: 'md',
};
