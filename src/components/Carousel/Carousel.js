import React, { Component } from 'react'
import PropTypes from 'prop-types';
import List from 'yallist';
import { TransitionGroup } from 'react-transition-group';
import { FaAngleLeft, FaAngleRight } from 'react-icons/fa';
import styles from './Carousel.module.css';
import getNode from './getNode';
import Arrow from './Arrow';
import ItemGroup from './ItemGroup';

export default class Carousel extends Component {
  static propTypes = {
    images: PropTypes.arrayOf(PropTypes.string),
    onChange: PropTypes.func,
    onItemClick: PropTypes.func,
  };

  static defaultProps = {
    images: [],
    onChange: () => {},
    onItemClick: () => {},
  };

  constructor(props) {
    super(props);
    const list = List.create(this.props.images);
    this.state = {
      list,
      image: list.head,
    };
  }

  transition = direction => node => event =>
    this.setState((state) => {
      const image = getNode(direction, node, state.list);
      this.props.onChange(image.value);
      return {
        image,
      };
    });

  onItemClick = image => {
    this.setState({ image });
    return this.props.onItemClick(image.value);
  }
  
  next = this.transition('next');

  prev = this.transition('prev');

  render() {
    const {
      next,
      onItemClick,
      prev,
      state: {
        image,
        list,
      },
    } = this;

    if (!list.length) {
      return null;
    }

    return (
      <div className={styles.carousel}>
        <Arrow
          icon={<FaAngleLeft />}
          onClick={prev(image)}
        />
        <TransitionGroup className={styles.carousel}>
          <ItemGroup
            node={image}
            head={list.head}
            tail={list.tail}
            onItemClick={onItemClick}
          />
        </TransitionGroup>
        <Arrow
          icon={<FaAngleRight />}
          onClick={next(image)}
        />
      </div>
    );
  }
}
