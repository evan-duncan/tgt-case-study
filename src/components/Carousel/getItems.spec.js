import List from 'yallist';
import getItems from './getItems';

const list = new List('foo', 'bar', 'baz', 'alice', 'bob');

describe('getItems', () => {
  it('should return an array of 3 items', () =>
    expect(getItems(list.head, list.head, list.tail)).toHaveLength(3));

  it('should put "bob" at the begining of the list if the current node is "foo"', () =>
    expect(getItems(list.head, list.head, list.tail)[0]).toEqual(list.tail));

  it('should put "foo" at the end of the list if the current node is "alice"', () =>
    expect(getItems(list.get(3), list.head, list.tail)[0]).toEqual(list.tail));
});
