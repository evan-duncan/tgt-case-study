
export default function getItems(node, head, tail) {
  const items = [];
  if (!node.prev) {
    items.push(tail);
  } else {
    items.push(node.prev);
  }

  items.push(node);

  if (!node.next) {
    items.push(head);
  } else {
    items.push(node.next);
  }

  return items;
}