import React from 'react';
import { Transition } from 'react-transition-group';
import { TweenLite, Linear } from 'gsap';
import classNames from 'classnames';
import styles from './Item.module.css';
import Image from '../Image';
import shouldShow from './shouldShow';

export default function Item({
  head,
  highlighted,
  item,
  node,
  onClick,
  tail,
}) {
  return (
    <Transition
      className={classNames(styles.item, {
        [styles.highlighted]: highlighted,
      })}
      onClick={() => onClick(node)}
      in={shouldShow(item, node, head, tail)}
      mountOnEnter
      unmountOnExit
      appear
      addEndListener={(node, done) => {
        TweenLite.to(node, 0, {
          lazy: true,
          y: 0,
          x: 0,
          autoAlpha: node ? 1 : 0,
          onComplete: done,
          ease: Linear.ease,
        });
      }}
    >
    <div>
      <Image src={item} alt="" thumbnail />
    </div>
    </Transition>
  );
}
