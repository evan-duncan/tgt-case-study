import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import getItems from './getItems';

export default function ItemGroup({
  head,
  node,
  onItemClick,
  tail,
}) {
  return (
    <div>
      {getItems(node, head, tail).map((n, idx) => 
        <Item
          onClick={onItemClick}
          key={idx}
          index={idx}
          item={n.value}
          node={n}
          head={head}
          tail={tail}
          highlighted={n.value === node.value}
        />)}
    </div>
  );
}

const Node = {
  value: PropTypes.any.isRequired,
  next: PropTypes.shape(this),
  prev: PropTypes.shape(this),
};

ItemGroup.propTypes = {
  node: PropTypes.shape(Node).isRequired,
  head: PropTypes.shape(Node).isRequired,
  tail: PropTypes.shape(Node).isRequired,
  onItemClick: PropTypes.func,
};

ItemGroup.defaultProps = {
  onItemClick: () => {},
};
