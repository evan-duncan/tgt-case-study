import List from 'yallist';
import shouldShow from './shouldShow';

const list = new List('foo', 'bar', 'baz', 'alice', 'bob');
describe('shouldShow', () => {
  it('should return true if item is node.value', () =>
    expect(shouldShow('foo', list.head, list.head, list.tail)).toBeTruthy());

  it('should return true if item is node.next.value', () =>
    expect(shouldShow('bar', list.head, list.head, list.tail)).toBeTruthy());

  it('should return true if item is "bob" and node is "foo"', () =>
    expect(shouldShow('bob', list.head, list.head, list.tail)).toBeTruthy());

  it('should return true if item is "foo" and node is "bob"', () =>
    expect(shouldShow('foo', list.tail, list.head, list.head)).toBeTruthy());

  it('should return false if item is "baz" and node is "foo"', () =>
    expect(shouldShow('baz', list.head, list.head, list.tail)).toBeFalsy());

  it('should return false if item is "alice" and node is "foo"', () =>
    expect(shouldShow('alice', list.head, list.head, list.tail)).toBeFalsy());
});
