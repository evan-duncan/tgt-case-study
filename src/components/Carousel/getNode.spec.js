import List from 'yallist';
import getNode from './getNode';


const list = new List('foo', 'bar', 'baz');
describe('getNode', () => {
  it('should return "bar" if direction is "next" and node is "foo"', () =>
    expect(getNode('next', list.head, list).value).toEqual('bar'));

  it('should return "foo" if direction is "next" and node is "baz"', () =>
    expect(getNode('next', list.tail, list).value).toEqual('foo'));

  it('should return "baz" if direction is "prev" and node is "foo"', () =>
    expect(getNode('prev', list.head, list).value).toEqual('baz'));

  it('should return "bar" if direction is "prev" and node is "baz"', () =>
    expect(getNode('prev', list.tail, list).value).toEqual('bar'));
});
