
export default function getNode(direction, node, list) {
  if (direction.toLowerCase() === 'next') {
    if (node.next) {
      return node.next;
    } else {
      return list.head;
    }
  }

  if (direction.toLowerCase() === 'prev') {
    if (node.prev) {
      return node.prev;
    } else {
      return list.tail;
    }
  }
}
