import React from 'react';
import PropTypes from 'prop-types';
import styles from './Arrow.module.css';

export default function Arrow({ icon, onClick }) {
  return (
    <button className={styles.arrow} type="button" onClick={onClick}>
      {icon}
    </button>
  );
}

Arrow.propTypes = {
  icon: PropTypes.node.isRequired,
  onClick: PropTypes.func,
}

Arrow.defaultProps = {
  onClick: () => {},
}
