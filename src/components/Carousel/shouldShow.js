import { get } from 'lodash';

export default function shouldShow(item, node, head, tail) {
  return [
    node.value,
    get(node, 'prev.value', tail.value),
    get(node, 'next.value', head.value),
  ].includes(item);
}