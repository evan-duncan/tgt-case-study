import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Loading.module.css';
import Logo from '../../logo.svg';

export default function Loading({ children, position }) {
  return (
    <div className={classNames(styles[position])}>
      <img className={styles.loading} src={Logo} alt="Loading indicator" />
      {children}
    </div>
  );
}

Loading.propTypes = {
  children: PropTypes.node,
  position: PropTypes.oneOf([
    'relative',
    'absolute',
  ]),
};

Loading.defaultProps = {
  children: null,
  default: 'relative',
};
