import React from 'react';
import renderer from 'react-test-renderer';
import Loading from './Loading';

describe('Loading', () => {
  [
    'absolute',
    'relative',
  ].forEach((test) =>
    it(`Should correctly render`, () => {
      const l = renderer
        .create(<Loading position={test}>Loading...</Loading>)
        .toJSON();
      expect(l).toMatchSnapshot();
    }));
});

