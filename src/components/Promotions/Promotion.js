import React from 'react'
import PropTypes from 'prop-types';

export default function Promotion({ promotionIdentifier, Description }) {
  return <li key={promotionIdentifier}>{Description[0].shortDescription}</li>;
}

export const shape = PropTypes.shape({
  Description: PropTypes.array,
  endDate: PropTypes.string,
  promotionIdentifier: PropTypes.string,
  promotionType: PropTypes.string,
  startDate: PropTypes.string,
});

Promotion.propTypes = shape;
