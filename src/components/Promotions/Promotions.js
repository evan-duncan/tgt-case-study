import React from 'react'
import PropTypes from 'prop-types';
import styles from './Promotions.module.css';
import Promotion, { shape as PromotionShape } from './Promotion';

export default function Promotions({ promotions }) {
  return (
    <ul className={styles.promotions}>
      {promotions.map(Promotion)}
    </ul>
  );
}

Promotions.propTypes = {
  promotions: PropTypes.arrayOf(PromotionShape),
}
