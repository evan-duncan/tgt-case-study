import React from 'react'
import PropTypes from 'prop-types';
import styles from './Divider.module.css';

export default function Divider({ vertical }) {
  return <div className={vertical ? styles.verticalDivider : styles.divider} />;
}

Divider.propTypes = {
  vertical: PropTypes.bool,
};

Divider.defaultProps = {
  vertical: false,
};
