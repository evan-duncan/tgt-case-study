import React from 'react'
import renderer from 'react-test-renderer';
import Divider from './Divider';

describe('Devider', () => {
  it('should render correctly', () => {
    const d = renderer
      .create(<Divider />)
      .toJSON();
    expect(d).toMatchSnapshot();
  });
});
