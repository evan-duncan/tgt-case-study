import React from 'react'
import PropTypes from 'prop-types';
import styles from './Highlights.module.css';
import { H1 } from '../Heading';

export default function Highlights({ headline, items }) {
  return (
    <div className={styles.highlights}>
      <H1>{headline}</H1>
      <ul>
        {items.map((item, idx) =>
          <li key={idx} dangerouslySetInnerHTML={{ __html: item}} />)}
      </ul>
    </div>
  );
}

Highlights.propTypes = {
  headline: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
};
