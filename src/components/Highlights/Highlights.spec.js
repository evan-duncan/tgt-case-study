import React from 'react'
import renderer from 'react-test-renderer';
import Highlights from './Highlights';

describe('Highlights', () => {
  it('should render correctly', () => {
    const h = renderer
      .create(
        <Highlights
          headline="Yo, dawg"
          items={["foo", "bar"]}
        />
      )
      .toJSON();
    expect(h).toMatchSnapshot();
  });
});
