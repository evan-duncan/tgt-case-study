import React, { Component } from 'react';
import { get } from 'lodash';
import MobileApp from './MobileApp';
import getDimensions from './utils/getDimensions';
import DesktopApp from './DesktopApp';

class App extends Component {

  constructor(props) {
    super(props);
    const { x } = getDimensions(document.getElementById('root'));
    this.state = {
      activeImage: null,
      images: [],
      isLoading: true,
      item: {},
      quantity: 1,
      x,
    };
  }

  async componentWillMount() {
    const response = await fetch('/api/items/072-04-1840');
    const data = await response.json();
    const item = get(data, 'CatalogEntryView[0]');
    const { PrimaryImage, AlternateImages } = get(item, 'Images[0]');

    this.setState({
      activeImage: get(item, 'Images[0].PrimaryImage[0]', { image: null }).image,
      con: get(item, 'CustomerReview[0].Con[0]'),
      consolidatedOverallRating: Number(get(item, 'CustomerReview[0].consolidatedOverallRating')),
      highlights: get(item, 'ItemDescription[0].features', []),
      images: [...PrimaryImage, ...AlternateImages].map(img => img.image),
      isLoading: false,
      item,
      offer: get(item, 'Offers[0].OfferPrice[0]'),
      pro: get(item, 'CustomerReview[0].Pro[0]'),
      promotions: get(item, 'Promotions'),
      purchasingChannelCode: Number(get(item, 'purchasingChannelCode')),
      returnPolicy: get(item, 'ReturnPolicy[0]'),
      reviews: get(item, 'CustomerReview[0].Reviews', []),
      title: get(item, 'title'),
      totalReviews: Number(get(item, 'CustomerReview[0].totalReviews')),
    });
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    const { x } = getDimensions(document.getElementById('root'));
    this.setState({ x });
  }

  updateQuantity = amt =>
    this.setState((state) =>
      ({ quantity: state.quantity += amt }));

  updateImage = activeImage =>
    this.setState({ activeImage });

  render() {
    return this.state.x < 1025
      ? <MobileApp {...this} {...this.state} {...this.props} />
      : <DesktopApp {...this} {...this.state} {...this.props} />
  }
}

export default App;
