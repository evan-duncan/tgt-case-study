import React from 'react';
import PropTypes from 'prop-types';
import styles from './CheckOutButtons.module.css';
import Button from '../../components/Button';

export default function CheckOutButtons({ purchasingChannelCode }) {
  return (
    <div className={styles.buttons}>
      {[0, 1].includes(purchasingChannelCode) &&
        <Button type="secondary">PICK UP IN STORE</Button>}

      {[0, 2].includes(purchasingChannelCode) &&
        <Button type="primary">ADD TO CART</Button>}
    </div>
  );
}

CheckOutButtons.propTypes = {
  purchasingChannelCode: PropTypes.number.isRequired,
};
