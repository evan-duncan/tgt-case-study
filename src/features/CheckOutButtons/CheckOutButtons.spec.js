import React from 'react';
import { mount } from 'enzyme';
import CheckOutButtons from './CheckOutButtons';
import Button from '../../components/Button';

describe('CheckOutButtons', () => {
  it('should have a "PICK UP IN STORE" button if the purchasingChannelCode is 1', () => {
    const buttons = mount(<CheckOutButtons purchasingChannelCode={1} />)
    expect(buttons.find(Button)).toHaveLength(1);
    expect(
      buttons
        .findWhere(e => e.text() === 'PICK UP IN STORE')
        .last()
        .type()
    ).toEqual('button');
  });

  it('should have an "ADD TO CART" button if purchasingChannelCode is 2', () => {
    const buttons = mount(<CheckOutButtons purchasingChannelCode={2} />)
    expect(buttons.find(Button)).toHaveLength(1);
    expect(
      buttons
        .findWhere(e => e.text() === 'ADD TO CART')
        .last()
        .type()
    ).toEqual('button');
  });

  it('should have a "PICK UP IN STORE" button and an "ADD TO CART" button if the purchasingChannelCode is 0', () => {
    const buttons = mount(<CheckOutButtons purchasingChannelCode={0} />);
    expect(buttons.find(Button)).toHaveLength(2);
    [
      'PICK UP IN STORE',
      'ADD TO CART',
    ].forEach(test => expect(
      buttons.find(Button).filterWhere(e => e.text() === test)
    ).toHaveLength(1));
  });
});
