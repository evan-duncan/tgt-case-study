import React, { Fragment } from 'react';
import { Grid, Col, Row } from 'react-flexbox-grid';
import styles from './App.module.css';
import asProductSection from './hocs/asProductSection';
import Title from './components/Title';
import Image from './components/Image';
import Loading from './components/Loading';
import Carousel from './components/Carousel';
import Promotions from './components/Promotions';
import Quantity from './components/Quantity';
import Highlights from './components/Highlights';
import CheckOutButtons from './features/CheckOutButtons';
import Reviews from './components/Reviews';
import ReturnPolicy from './components/ReturnPolicy/ReturnPolicy';
import Price from './components/Price';
import Actions from './components/Actions/Actions';

const PromotionsContainer = asProductSection(Promotions);
const PricingContainer = asProductSection(Fragment);

export default function DesktopApp({
  isLoading,
  title,
  activeImage,
  images,
  updateImage,
  offer,
  promotions,
  quantity,
  updateQuantity,
  purchasingChannelCode,
  returnPolicy,
  highlights,
  pro,
  con,
  reviews,
  consolidatedOverallRating,
  totalReviews,
}) {
  return (
    <div className={styles.App}>
      {isLoading
        ? <Loading position="absolute">Loading...</Loading>
        : <Grid fluid>
          <Row>
            <Col xs={6}>
              <div className={styles.centered}>
                <Title>{title}</Title>
                <Row>
                  <Col xs={12}>
                    <div className={styles.activeImage}>
                      <Image src={activeImage} alt="" zoomable />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <Carousel
                      images={images}
                      onChange={updateImage}
                      onItemClick={updateImage}
                    />
                  </Col>
                </Row>
              </div>
              <Reviews
                pro={pro}
                con={con}
                reviews={reviews}
                overall={consolidatedOverallRating}
                total={totalReviews}
              />
            </Col>
            <Col xs={6}>
              <PricingContainer>
                <Price
                  className={styles.alignTop}
                  amount={offer.formattedPriceValue}
                  channel={offer.priceQualifier}
                />
              </PricingContainer>
              <PromotionsContainer promotions={promotions} />
              <Quantity
                count={quantity}
                onDecrement={() => updateQuantity(-1)}
                onIncrement={() => updateQuantity(1)}
              />
              <CheckOutButtons purchasingChannelCode={purchasingChannelCode} />
              <ReturnPolicy {...returnPolicy} app={'#root'} />
              <Actions actions={[
                { text: 'add to registry', onClick: () => {} },
                { text: 'add to list', onClick: () => {} },
                { text: 'share', onClick: () => {} },
              ]} />
              <Highlights headline="product highlights" items={highlights} />
            </Col>
          </Row>
        </Grid>}
    </div>
  );
}
